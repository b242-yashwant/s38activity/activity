const mongoose = require("mongoose");

const courseSchema= new mongoose.Schema({
	name:{
		type: String,
		required:[true, "course is required"]
	},
	description:{
		type: String,
		required:[true, "description is required"]
	},
	price:{
		type: Number,
		required:[true, "price is required"]
	},
	isActive:{
		type:Boolean,
		default:true,
	},
	createdOn:{
		type:Date,
		default:new Date()
	},
	enrollees:[
	userId:{
		type:{String,
		required:[true,"userId is required"]
	     },
        enrolledOn:{
     	type: Date,
     	default: new Date()
        }
      }

	]
});