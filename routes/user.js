const express = require ("express");
const router = express.Router();
const userController=require("../controllers/user");
//router for checking if the users email already exists in the database

router.post("/checkEmail", (req, res)=>{
	userController.checkEmailExists(req.body).then(resultFromController=> res.send(resultFromController));
})
//Route for user registration
router.post("/register", (req, res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

//route for user authentication (login)
router.post("/login", (req, res)=>{
	userController.loginUser(req.body).then(resultFromController=>res.send(resultFromController));
})


//activity



router.post("/details",(req, res)=>{
	userController.userdetails(req.body).then(resultFromController=>res.send(resultFromController));
})
//allows us to export the "router" object that will be accessed in our index.js file
module.exports=router;