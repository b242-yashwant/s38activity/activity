const User = require("../models/User");
const bcrypt=require("bcrypt");
const auth = require("../auth");
//check if the email already exists
module.exports.checkEmailExists=(reqBody)=>{
	return User.find({email:reqBody.email}).then(result=>{
		//the "find" method returns a record if a match is found
		if (result.length>0){
			return true;
		}
		else{
			return false;
		}
	})
}

// User registration
module.exports.registerUser = (reqBody) => {
	// Creates a new user model
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		email : reqBody.email,
		mobileNo : reqBody.mobileNo,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// User registration failed
		if(error){
			return false;
		}
		// User registration successful
		else{
			return true;
		}
	})
}
//user Authentication (login)
module.exports.loginUser=(reqBody)=>{
	return User.findOne({email:reqBody.email}).then(result=>{
		//user does not exist
		if(result==null){
			return false
		}
		//user exist
		{
			const isPasswordcorrect=bcrypt.compareSync(reqBody.password, result.password);
			//if the password match/result of the code to true
			if(isPasswordcorrect){
				return{access: auth.createAccessToken(result)}
			}
			//password do not match
			else{
				return false;
			}
		}
	})
}

//activity


module.exports.userdetails=(reqBody)=>{
	return User.findOne({id:reqBody.id}).then(result=>{
		if(result==null){
			return false
		}
		else{
			result.password="";
			return result;
		}

	})
}
